package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    @Transactional
    @Modifying
    @Query(
            value = "DELETE FROM roles r WHERE r.code = :roleCode AND r.id NOT IN (SELECT u2r.role_id FROM users u INNER JOIN user2role u2r ON u.id = u2r.user_id)",
            nativeQuery = true
    )
    void deleteAllByNameAndEmpty(String roleCode);
}
