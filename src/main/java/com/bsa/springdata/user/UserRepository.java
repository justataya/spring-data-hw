package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findAllByLastName(String lastName, Pageable pageable);

    // GreaterThan and NOT Equal?
    List<User> findAllByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("SELECT u FROM User u WHERE u.office.city = :city ORDER BY u.lastName ASC")
    List<User> findAllByCity(String city);

    @Query("SELECT u FROM User u WHERE u.team.room = :room AND u.office.city = :city ORDER BY u.lastName ASC")
    List<User> findAllByRoomAndCity(String room, String city, Sort sort);

    // < or <= ?
    @Modifying
    @Query("DELETE FROM User u WHERE u.experience < :experience")
    int deleteByExperience(int experience);
}
