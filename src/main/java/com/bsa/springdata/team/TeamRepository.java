package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional<Team> findByName(String name);

    @Query("SELECT COUNT(t) FROM Team t WHERE t.technology.name = :technology")
    int countByTechnologyName(String technology);

    @Query(
            value = "SELECT * FROM teams t INNER JOIN users u ON t.id = u.team_id GROUP BY t.id, u.id HAVING COUNT(u.team_id) < :number limit 1",
            nativeQuery = true
    )
    Team findWithLowerUserNumber(int number);
}
