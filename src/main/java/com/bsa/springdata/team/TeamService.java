package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        Team team = teamRepository.findWithLowerUserNumber(devsNumber);
        Optional<Technology> technology = technologyRepository.findByName(newTechnologyName);

        assert technology.isPresent();

        team.setTechnology(technology.get());
        teamRepository.save(team);
    }

    public void normalizeName(String hipsters) {
        // Not sure what to do here
//        teamRepository.normalizeName(hipsters);
    }
}
