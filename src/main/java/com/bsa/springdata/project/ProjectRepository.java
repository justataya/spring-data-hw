package com.bsa.springdata.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query (
            value = "SELECT COUNT(ptuu2rri) FROM (SELECT DISTINCT p.id FROM projects p INNER JOIN teams t on p.id = t.project_id INNER JOIN users u on t.id = u.team_id INNER JOIN user2role u2r on u.id = u2r.user_id INNER JOIN roles r on u2r.role_id = r.id WHERE r.name = :role) as ptuu2rri",
            nativeQuery = true
    )
    int countProjectByUsersWithRole(String role);
}