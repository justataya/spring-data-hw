package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    Office findByAddress(String address);

    @Query("SELECT o FROM User u INNER JOIN u.office o INNER JOIN u.team t WHERE t.technology.name = :technology")
    List<Office> findAllByTechnology(String technology);
}
