package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfficeService {
    @Autowired
    private OfficeRepository officeRepository;

    public List<OfficeDto> getByTechnology(String technology) {
        return OfficeDto.fromList(officeRepository.findAllByTechnology(technology));
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
        Office office = officeRepository.getOne(
                officeRepository.findByAddress(oldAddress).getId()
        );

        office.setAddress(newAddress);
        officeRepository.save(office);

        return Optional.of(OfficeDto.fromEntity(office));
    }
}
